def vmul(v, s):
    """Multiply 2-dimensional vector by a scalar"""
    return (v[0] * s, v[1] * s)


def vadd(v1, v2):
    """Add two 2-dimensional vectors"""
    return (v1[0] + v2[0], v1[1] + v2[1])


def vsub(v1, v2):
    """Subtract v2 from v1"""
    return (v1[0] - v2[0], v1[1] - v2[1])


def quantize(value, qpu):
    """Quantize the value to the grid of `qpu` part per unit."""
    return round(value * qpu) / qpu


def in_bounds(pos, bounds):
    """Check if `pos` is within `bounds`

    `pos` is a tuple of x and y coordinates and `bounds` is a tuple of two
    tuples that contain minium x and y and maximum x and y respectively:

        pos = (x, y)
        bounds = (min, max)
        min = (minx, miny)
        max = (maxx, maxy)
    """
    x, y = pos
    (x0, y0), (x1, y1) = bounds
    return x0 <= x <= x1 and y0 <= y <= y1
