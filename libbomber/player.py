from .utils import vmul, vadd
from .items import ActiveItem, MovingItem


# Directions in which a player can go.
LEFT = 'Left'
RIGHT = 'Right'
UP = 'Up'
DOWN = 'Down'

# Initial speed of players
INIT_SPEED = 2


class Player(ActiveItem, MovingItem):
    """Human player"""

    max_speed = INIT_SPEED
    desired_directions = set()  # type: set[int]

    def set_desired_directions(self, directions):
        self.desired_directions = directions

    def update(self):
        super().update()
        if self.desired_directions:
            sd = (0, 0)
            for dd in self.desired_directions:
                sd = vadd(sd, {
                    LEFT: (-1, 0),
                    RIGHT: (1, 0),
                    UP: (0, -1),
                    DOWN: (0, 1)
                }[dd])
            self.speed = vmul(sd, self.max_speed)
        else:
            self.speed = (0, 0)
