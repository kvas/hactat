from .utils import quantize, vadd, vmul, vsub, in_bounds


class Field:
    """Playing field where all items are and all the action happens"""

    def __init__(self, width, height, ticks_per_second=10, pixels_per_unit=10):
        self.width = width
        self.height = height
        self.ticks_per_second = ticks_per_second
        self.pixels_per_unit = pixels_per_unit
        self.items = []

    def quantize_pos(self, pos):
        return (quantize(pos[0], self.pixels_per_unit),
                quantize(pos[1], self.pixels_per_unit))

    def add_item(self, item, pos):
        """Place item on the field in a specific position"""
        item.field = self
        item.pos = self.quantize_pos(pos)
        self.items.append(item)

    def get_items(self, pos):
        """Return all items that intersect the tile at (row, col)"""
        return [item for item in self.items if item.pos == pos]

    def move_items(self):
        """Move items according to speeds, detect collisions, update speeds"""
        dt = 1 / self.ticks_per_second
        q = self.quantize_pos
        bounds = ((0, 0), (self.width - 1, self.height - 1))

        blocking_items = []
        moving_items = []
        for item in self.items:
            if hasattr(item, 'speed') and item.speed != (0, 0):
                moving_items.append(item)
            if item.blocking:
                blocking_items.append(item)

        for item in moving_items:
            new_pos = q(vadd(item.pos, vmul(item.speed, dt)))

            if not in_bounds(new_pos, bounds):
                item.speed = (0, 0)
                continue

            for b in blocking_items:
                dx, dy = vsub(b.pos, new_pos)
                if abs(dx) < 1 and abs(dy) < 1:
                    item.speed = (0, 0)
                    break
            else:
                item.pos = new_pos

    def tick(self):
        """Run the time into the future for one tick

        One tick equals `(1 / ticks_per_second)` seconds. Will update
        positions of all objects according to their speeds, handle collisions,
        process explosions and other events.
        """
        for item in self.items:
            if hasattr(item, 'update'):
                item.update()
        self.move_items()
