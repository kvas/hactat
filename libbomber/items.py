class Item:
    """Generic item base class"""

    field = None  # type: Field
    pos = (0, 0)
    blocking = True

    def __repr__(self):
        return '<{} at {}>'.format(self.__class__.__name__, self.pos)


class ActiveItem(Item):
    """Item that can react to things"""

    def update(self):
        s = super()
        if hasattr(s, 'update'):
            s.update()


class MovingItem(Item):
    """Items that move according to their speed"""

    speed = (0, 0)
    blocking = False  # Moving items don't block


class Bomb(ActiveItem, MovingItem):
    """Bomb placed onto the field"""


class Wall(Item):
    """Impenetrable wall"""


class Explosion(ActiveItem):
    """Explosion in progress"""


class Brick(ActiveItem):
    """Bricks that can be destroyed"""
