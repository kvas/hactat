from hypothesis import given
import hypothesis.strategies as st

from libbomber import Field, Wall, Brick, Player, Bomb, LEFT, RIGHT, UP, DOWN


ITEM_CLASSES = {
    'W': Wall,
    '#': Brick,
    '@': Player,
    'o': Bomb,
}


def mk_field(*rows):
    field = Field(len(rows[0]), len(rows))
    for y, row in enumerate(rows):
        for x, cell in enumerate(row):
            if cell in ITEM_CLASSES:
                item_class = ITEM_CLASSES[cell]
                field.add_item(item_class(), (x, y))
    return field


def test_empty_field():
    field = Field(10, 10)
    assert field.width == 10
    assert field.height == 10
    for x in range(10):
        for y in range(10):
            assert field.get_items((x, y)) == []


def test_place_items():
    field = mk_field(' W o ',
                     ' # @ |')

    def check(pos, cls):
        items = field.get_items(pos)
        assert len(items) == 1
        assert isinstance(items[0], cls)

    check((1, 0), Wall)
    check((3, 0), Bomb)
    check((1, 1), Brick)
    check((3, 1), Player)


def test_move():
    field = mk_field(' W @ ')
    player = field.get_items((3, 0))[0]
    player.set_desired_directions([LEFT])
    assert player.pos == (3, 0)
    field.tick()
    assert player.pos == (2.8, 0)
    assert player.speed == (-2, 0)
    for i in range(5):
        field.tick()  # Collision should occur at some point.
    assert player.pos == (2, 0)
    assert player.speed == (0, 0)


ALL_DIRS = [LEFT, RIGHT, UP, DOWN]


@given(st.lists(st.sampled_from([LEFT, RIGHT, UP, DOWN] + list(range(20)))))
def test_random_running(moves):
    """Check that random running is handled in a sane way"""
    field = mk_field('     ',
                     ' W@W ',
                     '     ')
    player = field.get_items((2, 1))[0]
    desired_directions = []
    for i, cmd in enumerate(moves):
        if cmd in ALL_DIRS:
            if cmd in desired_directions:
                desired_directions.remove(cmd)
            else:
                desired_directions.append(cmd)
            player.set_desired_directions(desired_directions)
        else:
            for i in range(cmd):
                field.tick()

        x, y = player.pos
        assert 0 <= x <= 4  # Check that we're within the bounds...
        assert 0 <= y <= 3
        assert int(x) == x or int(y) == y  # ...and one coordinate is integer.
