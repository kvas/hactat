import setuptools


setuptools.setup(
    name='hactat',
    version='0.1.0',
    description='Hactat',
    long_description='',
    author='Bootwa',
    url='https://gitlab.com/bootwa/hactat',
    packages=['libbomber', 'hactat'],
    include_package_data=True,
    install_requires=[
        'pygame'
    ],
    entry_points={
        'console_scripts': [
            'hactat = hactat.__main__:main'
        ]
    },
    license='MIT',
    zip_safe=False,
    keywords='bomberman hactat',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: End Users/Desktop',
        'License :: OSI Approved :: MIT License',
        'Natural Language :: English',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.6',
    ],
)
